package youtube;

import java.util.ArrayList;
import java.util.Scanner;

public class Youtube {
	// ATRIBUTOS DE LA CLASE YOUTUBE
	private static ArrayList<Canal> canales;

	// CONSTRUCTOR DE LA CLASE YOUTUBE
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		canales = new ArrayList<Canal>();
		int opcion;
		do {
			mostrarMenu();
			while (!sc.hasNextInt()) {
				mostrarMenu();
				System.out.println("Selecciona una opción:");
				sc.next();
			}
			opcion = sc.nextInt();
			switch (opcion) {
				case 1:
					crearCanal();
					break;
				case 2:
					seleccionarCanal();
					break;
				case 3:
					mostrarEstadisticas();
					break;
				case 4:
					mostrarEstadisticasTotales();
					break;
				default:
					System.out.println("Elige entre una de las opciones");
			}

		} while (opcion != 0);

		System.out.println("Gracias! Adiós");
	}

	// METODOS DE LA CLASE YOUTUBE
	// MENU PRINCIPAL YOUTUBE
	private static void mostrarMenu() {
		System.out.println("| - - - YOUTUBE - - - |");
		System.out.println("1- Nuevo canal");
		System.out.println("2- Seleccionar Canal");
		System.out.println("3- Mostrar estadisticas");
		System.out.println("4- Mostrar estadisticas completas");
		System.out.println("0- Salir");
		System.out.println("|- - - - - - - - - - - - |");
		return;
	}

	// CREAR CANAL
	private static void crearCanal() {
		Scanner sc = new Scanner(System.in);
		Canal nuevoCanal; // Creamos un objeto de tipo canal
		System.out.println("Introduce el nombre del canal: ");
		String nombre = sc.nextLine();
		nuevoCanal = new Canal(nombre); // Instanciamos el objeto creado y le añadimos el nombre que hemos creado
		canales.add(nuevoCanal); // Añadimos el canal a la lista de canales
		nuevoCanal.menuCanal();// llamamos desde el mismo objeto creado a us menu
		return;
	}

	// SELECCIONAR CANAL
	private static void seleccionarCanal() {
		Scanner sc = new Scanner(System.in);
		int opcion;
		if (canales.isEmpty()) { // Comprobamos si tenemos canales en nuestro array de canales con .isEmpty() en
									// caso de que sea así nos muetsra un mensaje
			System.out.println("No hay canales, vamos a crear 1");
			crearCanal(); // llamamos al método 1 para crear un canal nuevo
		} else {
			do {
				System.out.println("Selecciona canal");
				Canal canalSeleccionado;
				for (int i = 0; i < canales.size(); i++) {
					canalSeleccionado = canales.get(i);
					System.out.println(i + "-Nombre del canal: " + canalSeleccionado.getNombre() + " creado en fecha: "
							+ canalSeleccionado.getFecha() + " con");
				}
				opcion = sc.nextInt();
				canalSeleccionado = canales.get(opcion);
				canalSeleccionado.menuCanal();

			} while (opcion < 0 || opcion > canales.size());

		}
		return;
	}

	// MOSTRAR ESTADISTICAS
	private static void mostrarEstadisticas() {
		// Llamamos al orrayList canales y usamos la funcion .size() que nos devuelve la
		// cantidad de objetos que hay en su interior
		System.out.println("Yotube tiene " + canales.size() + " canales");
		return;
	}

	// MOSTRAR ESTADISTICAS TOTALES
	private static void mostrarEstadisticasTotales() {
		System.out.println("Youtube tiene " + canales.size() + " canales");
		Canal estadisticasCanal;
		for (int i = 0; i < canales.size(); i++) {
			estadisticasCanal = canales.get(i);
			System.out.println(i + "-Nombre del canal: " + estadisticasCanal.getNombre() + " creado en fecha: "
					+ estadisticasCanal.getFecha());
			Video estadisticasVideo;
			for (int j = 0; j < estadisticasCanal.videos.size(); j++) {
				estadisticasVideo = estadisticasCanal.videos.get(j);
				System.out.println("-- Video: " + estadisticasVideo.getNombreVideo() + " en fecha "
						+ estadisticasVideo.getFechaVideo() + " con " + estadisticasVideo.getNumeroLikes() + " likes y "
						+ estadisticasVideo.comentarios.size() + " comentarios ");
				Comentario mostrarComentario;
				for (int z = 0; z < estadisticasVideo.comentarios.size(); z++) {
					mostrarComentario = estadisticasVideo.comentarios.get(z);
					System.out.println("--- Comentario: " + mostrarComentario.getComentario() + "del usuario"
							+ mostrarComentario.getUsuarioNombre() + " en fecha "
							+ mostrarComentario.getFechaComentario());
				}
			}
		}

	}

	// GETTERS Y SETTERS PARA LOS ATRIBUTOS DEL OBJETO YOUTUBE
	public static ArrayList<Canal> getCanales() {
		return canales;
	}

	public static void setCanales(ArrayList<Canal> canales) {
		Youtube.canales = canales;
	}

}
