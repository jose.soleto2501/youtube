package youtube;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class Video {
	// ATRIBUTOS DE LA CLASE VIDEO
	private String nombreVideo;
	private int numeroLikes;
	private String fechaVideo;
	ArrayList<Comentario> comentarios;
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";

	// CONSTRUCTOR DE LA CLASE VIDEO
	public Video(String nombreVideo) {
		setNombre(nombreVideo);
		setNumeroLikes(0);
		setFechaVideo();
		comentarios = new ArrayList<Comentario>();
	}

	public void menuVideo() {
		Scanner sc = new Scanner(System.in);
		int opcion;
		do {
			mostrarMenuVideo();
			while (!sc.hasNextInt()) {
				mostrarMenuVideo();
				System.out.println("Selecciona una opción:");
				sc.next();
			}
			opcion = sc.nextInt();
			switch (opcion) {
				case 1:
					crearComentario();
					break;
				case 2:
					likeVideo();
					break;
				case 3:
					mostrarComentariosVideo();
					break;
				case 4:
					mostrarInfoVideo();
					break;
				default:
					System.out.println("Elige entre una de las opciones");
			}

		} while (opcion != 0);
		return;
	}

	// METODOS DE LA CLASE VIDEO
	private void mostrarMenuVideo() {
		System.out.println("| - - - " + getNombreVideo() + " - - - |");
		System.out.println("1- Nuevo comentario");
		System.out.println("2- Like");
		System.out.println("3- Mostrar Comentarios");
		System.out.println("4- Mostrar estadísticas");
		System.out.println("0- Salir");
		System.out.println("|- - - - - - - - - - - - |");
		return;
	}

	// CREAR COMENTARIO
	private void crearComentario() {
		Scanner sc = new Scanner(System.in);
		String comentario;
		String usuario;
		System.out.println("Escribe el comentario: ");
		comentario = sc.nextLine();
		System.out.println("Introduce el nombre del usuario");
		usuario = sc.nextLine();
		Comentario nuevoComentario = new Comentario(comentario, usuario);
		comentarios.add(nuevoComentario);
		return;
	}

	// DAR LIKE AL VIDEO
	private void likeVideo() {
		// Llamamos para dar el nuevo valor de likes que será el antiguo (usando
		// getNumeroLikes que nos devolverá la cantidad) y le sumamos 1
		setNumeroLikes(getNumeroLikes() + 1);
		System.out.println(ANSI_RED + "ME GUSTA" + ANSI_RESET);
		return;
	}

	// MOSTRAR LOS COMENTARIOS
	private void mostrarComentariosVideo() {
		for (int i = 0; i < comentarios.size(); i++) {
			Comentario mostrarComentario = comentarios.get(i);
			System.out.println("Comentario: " + mostrarComentario.getComentario() + " del usuario "
					+ mostrarComentario.getUsuarioNombre() + " en fecha " + mostrarComentario.getFechaComentario());
		}
		return;
	}

	// MOSTRAR INFO DEL VIDEO
	private void mostrarInfoVideo() {
		System.out.println("Video: " + getNombreVideo() + " en fecha " + getFechaVideo() + " con " + getNumeroLikes()
				+ " likes y " + comentarios.size() + " comentarios");
		return;
	}

	// GETTERS Y SETTERS DE LA CLASE OBJETO VIDEO
	public String getNombreVideo() {
		return nombreVideo;
	}

	public void setNombre(String nombreVideo) {
		this.nombreVideo = nombreVideo;
	}

	public int getNumeroLikes() {
		return numeroLikes;
	}

	public void setNumeroLikes(int numeroLikes) {
		this.numeroLikes = numeroLikes;
	}

	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(ArrayList<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public String getFechaVideo() {
		return fechaVideo;
	}

	public void setFechaVideo() {
		LocalDateTime fecha = LocalDateTime.now();
		DateTimeFormatter fechaConFormato = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		fechaVideo = fecha.format(fechaConFormato);
	}

}
