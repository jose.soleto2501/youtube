package youtube;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Comentario {
    private String comentario;
    private String usuarioNombre;
    private String fechaComentario;

    public Comentario(String comentario, String usuarioNombre) {
        setComentario(comentario);
        setUsuarioNombre(usuarioNombre);
        setFechaComentario();
        
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getFechaComentario() {
		return fechaComentario;
	}

	public void setFechaComentario() {
		LocalDateTime fecha = LocalDateTime.now();
		DateTimeFormatter fechaConFormato = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		fechaComentario = fecha.format(fechaConFormato);
	}
}
