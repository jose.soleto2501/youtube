package youtube;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class Canal {
	// ATRIBUTOS DE LA CLASE CANAL
	private String nombre;
	private String fecha;
	ArrayList<Video> videos;

	// CONSTRUCTOR DE LA CLASE CANAL
	public Canal(String nombre) {
		setNombre(nombre);
		setFecha();
		videos = new ArrayList<Video>();
	}

	// MÉTODOS DE LA CLASE CANAL
	// MENU PRINCIPAL CANAL
	public void menuCanal() {
		Scanner sc = new Scanner(System.in);
		int opcion;
		do {
			mostrarMenuCanal();
			while (!sc.hasNextInt()) {
				mostrarMenuCanal();
				System.out.println("Selecciona una opción:");
				sc.next();
			}
			opcion = sc.nextInt();
			switch (opcion) {
				case 1:
					crearVideo();
					break;
				case 2:
					seleccionarVideo();
					break;
				case 3:
					mostrarEstadisticasCanal();
					break;
				case 4:
					mostrarInfoVideos();
					break;
				default:
					System.out.println("Elige entre una de las opciones");
			}
		} while (opcion != 0);
		return;
	}

	// CREAR VIDEO
	void crearVideo() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el nombre del video");
		String nombre = sc.nextLine();
		Video videoNuevo = new Video(nombre);
		videos.add(videoNuevo);
		videoNuevo.menuVideo();
		return;
	}

	// SELECCIONAR VIDEO
	void seleccionarVideo() {
		Scanner sc = new Scanner(System.in);
		int opcion;
		if (videos.isEmpty()) {
			System.out.println("No hay videos, vamos a crear 1");
			crearVideo();
		} else {
			do {
				System.out.println("Selecciona Video: ");
				Video videoSeleccionado;
				for (int i = 0; i < videos.size(); i++) {
					videoSeleccionado = videos.get(i);
					System.out.println(i + "-Nombre del video: " + videoSeleccionado.getNombreVideo() + " en fecha "
							+ getFecha() + " con " + videoSeleccionado.getNumeroLikes() + " likes y "
							+ videoSeleccionado.getComentarios() + " comentarios ");
				}
				opcion = sc.nextInt();
				videoSeleccionado = videos.get(opcion);
				videoSeleccionado.menuVideo();

			} while (opcion < 0 || opcion > videos.size());
		}
		return;
	}
	// MOSTRAR MENU CANAL

	private void mostrarMenuCanal() {
		System.out.println("|---" + getNombre() + "---|");
		System.out.println("1- Nuevo video");
		System.out.println("2- Seleccionar Video");
		System.out.println("3- Mostrar estadisticas");
		System.out.println("4- Mostrar info videos");
		System.out.println("0- Salir");
		System.out.println("|-------------------|");
	}

	// MOSTRAR ESTADISTICAS CANAL
	private void mostrarEstadisticasCanal() {
		System.out.println(
				"Nombre del canal: " + getNombre() + " en fecha" + getFecha() + " con " + videos.size() + " videos");
		return;
	}

	// MOSTRAR INFO VIDEOS
	private void mostrarInfoVideos() {
		for (int i = 0; i < videos.size(); i++) {
			Video mostrarVideos = videos.get(i);
			;
			System.out.println("Video: " + mostrarVideos.getNombreVideo() + " en fecha " + mostrarVideos.getFechaVideo()
					+ " con " + mostrarVideos.getNumeroLikes() + " likes y " + mostrarVideos.comentarios.size()
					+ " comentarios");
		}
		return;
	}

	// GETTES Y SETTERS DEL OBJETO CANAL
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha() {
		LocalDateTime fecha = LocalDateTime.now();
		DateTimeFormatter fechaConFormato = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.fecha = fecha.format(fechaConFormato);
	}

}
